<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pinjam_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	
	public function get_data_pinjam()
	{
		$this->db->select('*');
		$this->db->from('pinjam');
		$this->db->join('petugas', 'petugas.ID_PETUGAS = pinjam.ID_PETUGAS');
		$this->db->join('anggota', 'anggota.ID_USER = pinjam.ID_USER');
		//$this->db->join('detail_pinjam', 'detail_pinjam.NO_PINJAM = pinjam.NO_PINJAM');
		$this->db->join('buku', 'buku.KD_BUKU = pinjam.KD_BUKU');
		$this->db->order_by('pinjam.TANGGAL', 'ASC')->where('status','Belum Kembali');

		return $this->db->get()->result();

		//return $this->db->order_by('NO_PINJAM','ASC')->get('pinjam')->result();
	}

	public function get_data_kembali()
	{
		$this->db->select('*');
		$this->db->from('pinjam');
		$this->db->join('petugas', 'petugas.ID_PETUGAS = pinjam.ID_PETUGAS');
		$this->db->join('anggota', 'anggota.ID_USER = pinjam.ID_USER');
		//$this->db->join('detail_pinjam', 'detail_pinjam.NO_PINJAM = pinjam.NO_PINJAM');
		$this->db->join('buku', 'buku.KD_BUKU = pinjam.KD_BUKU');
		$this->db->order_by('pinjam.TANGGAL', 'ASC')->where('status !=','Belum Kembali');

		return $this->db->get()->result();

		//return $this->db->order_by('NO_PINJAM','ASC')->get('pinjam')->result();
	}

	public function simpan()
	{
		$data = array(
				'ID_PETUGAS'		=> $this->session->userdata('id'),
				'ID_USER'			=> $this->input->post('nama'),
				'KD_BUKU'			=> $this->input->post('buku'),
				'TANGGAL'			=> $this->input->post('tgl_pinjam'),
				'DEADLINE' 			=> $this->input->post('tgl_kembali'),
				'DENDA'				=> '0',
				'STATUS'			=> 'Belum Kembali'
			);

		$stock = 1;
		$this->db->where('KD_BUKU', $this->input->post('buku'));
		$this->db->set('JUMLAH', 'JUMLAH - ' . (int) $stock, FALSE);
		$this->db->update('buku');

		$this->db->insert('pinjam', $data);
		//$this->db->insert('detail_pinjam', $data_detail);

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function kembali($id,$denda,$status,$buku)
	{
		$data = array(
					//'NO_PINJAM'			=> 
					'DENDA'				=> $denda,
					'STATUS'			=> $status,
					'KEMBALI' 			=> date('Y-m-d')
			);

		$this->db->where('NO_PINJAM', $id)->update('pinjam', $data);

		$stock = 1;
		$this->db->where('KD_BUKU', $buku);
		$this->db->set('JUMLAH', 'JUMLAH + ' . (int) $stock, FALSE);
		$this->db->update('buku');

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function getBuku($id)
	{
		$this->db->select('KD_BUKU')->from('pinjam')->where('NO_PINJAM', $id);
		$query = $this->db->get();
		if($query->num_rows() == 1){
			$sql = $query->row();
			return $sql->KD_BUKU;
		}
	}

}

/* End of file pinjam_model.php */
/* Location: ./application/models/pinjam_model.php */