<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petugas_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function get_data_petugas(){
		return $this->db->order_by('ID_PETUGAS','ASC')->get('petugas')->result();
	}

	public function simpan()
	{
		$data = array(
				'USERNAME'		=> $this->input->post('username'),
				'PASSWORD'		=> $this->input->post('password'),
				'NAMAP'			=> $this->input->post('nama'),
				'ROLE'			=> $this->input->post('role')
			);

		$this->db->insert('petugas',$data);

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function edit($id)
	{
		$data = array(
				'USERNAME'		=> $this->input->post('username'),
				'PASSWORD'		=> $this->input->post('password'),
				'NAMAP'			=> $this->input->post('nama'),
				'ROLE'			=> $this->input->post('role')
			);

		$this->db->where('ID_PETUGAS',$id)->update('petugas',$data);

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function delete($id)
	{
		$this->db->where('ID_PETUGAS',$id)
				 ->delete('petugas');

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function get_detil_petugas($id)
	{
		return $this->db->get_where('petugas',array('ID_PETUGAS'->$id));
	}

}

/* End of file petugas_model.php */
/* Location: ./application/models/petugas_model.php */