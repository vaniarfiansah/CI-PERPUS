<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	
	public function cek_user(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$query = $this->db->where('USERNAME',$username)
						  ->where('PASSWORD',$password)
						  ->get('petugas');

		if($query->num_rows() > 0){
			//$petugas = array_shift($query->result_array());
			$petugas = array_shift($query->result_array());
			//$this->db->where('ROLE',$role)->get('petugas');
			$data = array('USERNAME' => $username, 'logged_in' => TRUE, 'id' => $petugas['ID_PETUGAS'], 'role' => $petugas['ROLE']);
			$this->session->set_userdata($data);

			return TRUE;
		} else {
			return FALSE;
		}
	}

}

/* End of file admin_model.php */
/* Location: ./application/models/admin_model.php */