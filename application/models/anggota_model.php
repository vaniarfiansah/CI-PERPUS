<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function daftar()
	{
		$data = array(
				'NIS'		=> $this->input->post('nis'),
				'NAMA'		=> $this->input->post('nama'),
				'KELAS'		=> $this->input->post('kelas'),
				'JK'		=> $this->input->post('jk'),
				'NO_HP'		=> $this->input->post('nohp')
			);

		$this->db->insert('anggota',$data);

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function edit($id)
	{
		$data = array(
				'NIS'		=> $this->input->post('nis'),
				'NAMA'		=> $this->input->post('nama'),
				'KELAS'		=> $this->input->post('kelas'),
				'JK'		=> $this->input->post('jk'),
				'NO_HP'		=> $this->input->post('nohp')
			);

		$this->db->where('ID_USER',$id)->update('anggota',$data);

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	// public function get_detil_anggota($id)
	// {
	// 	return $this->db->get_where('anggota',array('ID_USER'->$id));
	// }

	public function get_data_anggota()
	{
		return $this->db->order_by('ID_USER','ASC')
						->get('anggota')
						->result();
	}

	public function total_records()
	{
		return $this->db->from('anggota')
						->count_all_result();
	}

	public function delete($id_anggota)
	{
		$this->db->where('ID_USER',$id_anggota)
				 ->delete('anggota');

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

}

/* End of file anggota_model.php */
/* Location: ./application/models/anggota_model.php */