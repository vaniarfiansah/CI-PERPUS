<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buku_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function simpan($foto){
		$data = array(
				'JUDUL'			=> $this->input->post('judul'),
				'KATEGORI'		=> $this->input->post('kategori'),
				'PENULIS'		=> $this->input->post('penulis'),
				'PENERBIT'		=> $this->input->post('penerbit'),
				'JUMLAH'		=> $this->input->post('jumlah'),
				'FOTO'			=> $foto['file_name']
			);

		$this->db->insert('buku',$data);

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function edit($id){
		$data = array(
				'JUDUL'			=> $this->input->post('judul'),
				'KATEGORI'		=> $this->input->post('kategori'),
				'PENULIS'		=> $this->input->post('penulis'),
				'PENERBIT'		=> $this->input->post('penerbit')
			);

		$this->db->where('KD_BUKU',$id)->update('buku',$data);

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}

	}

	// public function get_detil_buku($id)
	// {
	// 	return $this->db->get_where('buku',array('KD_BUKU'->$id));
	// }

	public function get_data_buku()
	{
		return $this->db->order_by('KD_BUKU','ASC')->get('buku')->result();
	}

	public function delete($id)
	{
		$this->db->where('KD_BUKU',$id)
				 ->delete('buku');

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/*public function getStockBuku($buku)
	{
		$this->db->select('JUMLAH')->from('buku')->where('KD_BUKU',$id);
		$query = $this->db->get();
		if($query->num_rows() == 1){
			$sql = $query->row();
			return $sql->JUMLAH;
		}
	}

	public function kurangStock($buku,$dipinjam)
	{
		$object = array('JUMLAH' => $dipinjam);
		$this->db->where('KD_BUKU', $buku)->update('buku', $object);
	}*/

}

/* End of file buku_model.php */
/* Location: ./application/models/buku_model.php */