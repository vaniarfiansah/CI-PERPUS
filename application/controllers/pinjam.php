<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pinjam extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('anggota_model');
		$this->load->model('buku_model');
		$this->load->model('pinjam_model');
	}
	
	public function index()
	{
		if($this->session->userdata('logged_in') == TRUE){
			$data['anggota'] = $this->anggota_model->get_data_anggota();
			$data['buku'] = $this->buku_model->get_data_buku();
			$data['pinjam'] = $this->pinjam_model->get_data_pinjam();
			$data['main_view'] = 'pinjam_view';
			$this->load->view('template', $data);
		} else {
			redirect('admin');
		}
	}

	public function simpan()
	{
		if($this->pinjam_model->simpan() == TRUE){
			$this->session->set_flashdata('notif', 'Peminjaman Berhasil');
			redirect('pinjam');
		} else {
			$this->session->set_flashdata('notif', 'Peminjaman Gagal');
			redirect('pinjam');
		}
	}

	public function kembali($id,$denda)
	{
		if($denda == 0){
			$status = 'Tepat Waktu';
		} else {
			$status = 'Terlambat';
		}

		$buku = $this->pinjam_model->getBuku($id);

		if($this->pinjam_model->kembali($id,$denda,$status,$buku) == TRUE){
			$this->session->set_flashdata('notif', 'Pengembalian Berhasil');
			redirect('pinjam');
		} else {
			$this->session->set_flashdata('notif', 'Pengembalian Gagal');
			redirect('pinjam');
		}
	}

}

/* End of file pinjam.php */
/* Location: ./application/controllers/pinjam.php */