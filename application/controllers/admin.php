<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('admin_model');
	}

	public function index()
	{
		if($this->session->userdata('logged_in') == TRUE){
			redirect(base_url('index.php/admin/dashboard'));
		} else {
			$this->load->view('login_view');
		}

		/*$data['main_view'] = 'dashboard_view';
		$this->load->view('template',$data);*/
	}

	public function dashboard(){
		if($this->session->userdata('logged_in') == TRUE){
			$data['main_view'] = 'dashboard_view';
			$this->load->view('template', $data);
		} else {
			redirect('admin');
		}
	}

	public function masuk(){
		if($this->admin_model->cek_user() == TRUE){
			redirect(base_url('index.php/admin/dashboard'));
		} else {
			$data['notif'] = 'login gagal';
			$this->load->view('login_view', $data);
		}
	}

	public function logout(){
		$data = array('username' => '', 'logged_in' => FALSE);

		$this->session->sess_destroy();
		$this->load->view('login_view');
	}
}

/* End of file controllername.php */
/* Location: ./application/controllers/controllername.php */