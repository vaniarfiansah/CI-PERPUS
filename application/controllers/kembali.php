<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kembali extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('anggota_model');
		$this->load->model('buku_model');
		$this->load->model('pinjam_model');
	}

	public function index()
	{
		if($this->session->userdata('logged_in') == TRUE){
			$data['anggota'] = $this->anggota_model->get_data_anggota();
			$data['buku'] = $this->buku_model->get_data_buku();
			$data['pinjam'] = $this->pinjam_model->get_data_kembali();
			$data['main_view'] = 'kembali_view';
			$this->load->view('template', $data);
		} else {
			redirect('admin');
		}
	}

}

/* End of file kembali.php */
/* Location: ./application/controllers/kembali.php */