<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_pinjam extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$data['main_view'] = 'laporan_pinjam_view';
		$this->load->view('template', $data);
	}

}

/* End of file laporan_pinjam.php */
/* Location: ./application/controllers/laporan_pinjam.php */