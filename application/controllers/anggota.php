<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('admin_model');
		$this->load->model('anggota_model');

	}

	public function index()
	{
		if($this->session->userdata('logged_in') == TRUE){
			$data['anggota'] = $this->anggota_model->get_data_anggota();
			$data['main_view'] = 'anggota_view';
			$this->load->view('template', $data);
		} else {
			redirect('admin');
		}	
	}

	public function daftar(){
		if($this->anggota_model->daftar() == TRUE){
			$this->session->set_flashdata('notif1', 'Pendaftaran Berhasil');
            redirect('anggota');
		} else {
			$this->session->set_flashdata('notif1', 'Pendaftaran Gagal');
            redirect('anggota');
		}
	}

	public function hapus($id_anggota)
	{
		if($this->anggota_model->delete($id_anggota) == TRUE){
			$this->session->set_flashdata('notif', 'Anggota Berhasil Dihapus');
			redirect('anggota');
		} else {
			$this->session->set_flashdata('notif', 'Anggota Gagal Dihapus');
			redirect('anggota');
		}
	}

	public function edit($id)
	{
		if($this->anggota_model->edit($id) == TRUE){
			$this->session->set_flashdata('notif', 'Edit data berhasil');
			redirect('anggota');
		} else {
			$this->session->set_flashdata('notif', 'Edit data gagal');
            redirect('anggota');
		}
	}

	public function detil($id)
	{
		$data['detil'] = $this->anggota_model->get_detil_anggota($id)->result_object();
	}

}

/* End of file anggota.php */
/* Location: ./application/controllers/anggota.php */