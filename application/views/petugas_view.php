<div class="col-lg-12">
	<h1 class="page-header">Petugas</h1>
</div>
<div class="row col-lg-4">
<form method="post" action="<?php echo base_url(); ?>index.php/petugas/simpan" id="form-buku">
    <h3 style="text-align: center;">Tambah Petugas</h3>
    <?php
        $notif = $this->session->flashdata('notif1');

        if(!empty($notif)){
            echo '<div class="alert alert-success">';
            echo $notif;
            echo '</div>';
        }
    ?>
    	<div class="form-group">
    		<label for="username">Username:</label>
    		<input type="text" class="form-control" name="username" placeholder="Username" required>
    	</div>
    	<div class="form-group">
    		<label for="password">Password:</label>
    		<input type="password" class="form-control" name="password" placeholder="Password" required>
    	</div>
        <div class="form-group">
            <label for="username">Nama:</label>
            <input type="text" class="form-control" name="nama" placeholder="Nama" required>
        </div>
        <div class="form-group">
        <label>Role:</label>
        <select name="role" class="form-control">
            <option value="admin">Admin</option>
            <option value="super">Super Admin</option>
        </select>
        </div>
    	<div class="row">
    		<div class="col-lg-6">
    		<input type="reset" name="reset" value="RESET" class="btn btn-block btn-md btn-danger">
    		</div>
    		<div class="col-lg-6">
    		<input type="submit" name="submit" value="SIMPAN" class="btn btn-block btn-md btn-primary">
    		</div>
    	</div>
    	</form>
	</div>

<div class="col-lg-8">
	<h3 style="text-align: center;">Data Petugas</h3>
    <?php
        $notif = $this->session->flashdata('notif');

        if(!empty($notif)){
        echo '<div class="alert alert-success">'.$notif.'</div>';
        }
    ?>
	 		<!-- <div class="panel panel-default">
                        <div class="panel-heading">
                            DataTables Advanced Tables
                        </div> -->
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Username</th>
                                            <th>Password</th>
                                            <th>Nama</th>
                                            <th>Role</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                        $no = 1;
                                        foreach ($petugas as $data) {
                                            echo "
                                            <tr class='odd gradeX'>
                                            <td>".$no++."</td>
                                            <td>$data->USERNAME</td>
                                            <td>$data->PASSWORD</td>
                                            <td>$data->NAMAP</td>
                                            <td>$data->ROLE</td>
                                            <td><button class='btn btn-success glyphicon glyphicon-edit' data-toggle='modal' data-target='#modal$data->ID_PETUGAS'></button>
                                            <a href='".base_url()."index.php/petugas/hapus/$data->ID_PETUGAS' type='button' class='btn btn-danger glyphicon glyphicon-trash'></a></td>
                                            </tr>
                                            <!-- Modal -->
                                        <div class='modal fade' id='modal$data->ID_PETUGAS' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
                                            <div class='modal-dialog'>
                                                <div class='modal-content'>
                                                  <form role='form' action='".base_url()."index.php/petugas/edit/".$data->ID_PETUGAS."' method='post'>
                                                    <div class='modal-header'>
                                                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                                        <h4 class='modal-title' id='myModalLabel'>Edit Data Petugas</h4>
                                                    </div>
                                                    <div class='modal-body'>";?>
                                                    <div class="form-group">
                                                        <label for="username">Username:</label>
                                                        <input type="text" class="form-control" name="username" value="<?php echo $data->USERNAME?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="password">Password:</label>
                                                        <input type="password" class="form-control" name="password" value="<?php echo $data->PASSWORD?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="username">Nama:</label>
                                                        <input type="text" class="form-control" name="nama" value="<?php echo $data->NAMAP?>">
                                                    </div>
                                                    <div class="form-group">
                                                    <label>Role:</label>
                                                    <select name="role" class="form-control">
                                                        <option value="admin">Admin</option>
                                                          <option value="super" <?php if($data->ROLE=='super'){echo 'selected';} ?>>Super</option>
                                                    </select>
                                                    </div>
                                                    <?php echo "</div>
                                                    <div class='modal-footer'>
                                                    <div class='row'>
                                                    <div class='col-lg-6'>
                                                    <input type='button' name='cancel' class='btn btn-danger btn-block' data-dismiss='modal' value='Cancel'>
                                                    </div>
                                                    <div class='col-lg-6'>
                                                    <input type='submit' name='submit' class='btn btn-success btn-block' value='Edit'>
                                                    </div>
                                                </div>

                                                    </form>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <!-- /.modal -->
                                      ";
                                    }
                                  ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    <!-- </div> -->

</div>
