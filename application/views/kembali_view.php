 	<div class="col-lg-12">
        <h1 class="page-header" style="text-align: center;">Data Pengembalian</h1>
    </div>

<div class="col-lg-12">
	<!-- <h3 style="text-align: center;">Data Pengembalian</h3>
	 		 <div class="panel panel-default">
                        <div class="panel-heading">
                            DataTables Advanced Tables
                        </div> -->
                        <!-- /.panel-heading -->

                        <?php
                            $notif = $this->session->flashdata('notif');

                            if(!empty($notif)){
                                echo '<div class="alert alert-success">'.$notif.'</div>';
                            }
                        ?>
                        <!-- /.panel-heading -->
                <button class="btn btn-primary" onclick="window.print()" style="margin-bottom: 8px">Print</button>
            <div class="row">
                <div class="col-lg-12">
                        
                        <div class="panel panel-default">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                        	<th>No</th>
                                            <th>Nama</th>
                                            <th>Judul</th>
                                            <th>Tanggal Pinjam</th>
                                            <th>Deadline</th>
                                            <th>Tanggal Kembali</th>
                                            <th>Petugas</th>
                                            <th>Denda</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                    	$no = 1;
                                        foreach ($pinjam as $data) {
                                            $deadline = strtotime($data->DEADLINE);
                                            $sekarang = strtotime($data->KEMBALI);
                                            $kembali = ($sekarang-$deadline)/86400;

                                            if($kembali < 0 ){
                                                $kembali = 0;
                                            }

                                        echo '
                                        <tr class="odd gradeX">
                                        	<td>'.$no.'</td>
                                            <td>'.$data->NAMA.'</td>
                                            <td>'.$data->JUDUL.'</td>
                                            <td>'.$data->TANGGAL.'</td>
                                            <td>'.$data->DEADLINE.'</td>
                                            <td>'.$data->KEMBALI.'</td>
                                            <td>'.$data->NAMAP.'</td>
                                            <td>Rp'.$data->DENDA.'</td>
                                            <td>'.$data->STATUS.' '; if($data->STATUS != 'Tepat Waktu'){echo $kembali .' hari';} echo'</td>
                                        </tr>
                                        '; $no++;
                                        }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                    <!-- </div> -->

</div>
