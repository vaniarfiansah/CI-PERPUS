 	<div class="col-lg-12">
        <h1 class="page-header">Peminjaman</h1>
    </div>
    <div class="row col-lg-3">
    <h3 style="text-align: center;">Pinjam Buku</h3>
    <form method="post" action="<?php echo base_url(); ?>index.php/pinjam/simpan" id="form-pinjam">
     <?php
        if(!empty($notif)){
        	echo '<div class="alert alert-danger">';
        	echo $notif;
        	echo '</div>';
        }
    ?>
    	<label>Nama:</label>
    	<select class="form-control" name="nama">
    	<?php
    		foreach ($anggota as $dataA) {
    			echo '<option value="'.$dataA->ID_USER.'">'.$dataA->NAMA.'</option>'
    		;}
    	?>
    	</select>
    	<br>
    	<label>Buku:</label>
    	<select class="form-control" name="buku">
    	<?php
    		foreach ($buku as $dataB) {
    			if($dataB->JUMLAH != 0){echo '<option value="'.$dataB->KD_BUKU.'">'.$dataB->JUDUL.'</option>';}
    		}
    	?>
    	</select>
    	<br>
    	<div class="form-group">
    	<label>Tanggal Pinjam:</label>
    	<input class="form-control" type="text" value="<?php echo date('Y-m-d')?>" name="tgl_pinjam" readonly>
    	</div>
        <!-- <div class="form-group">
            <label>Tanggal Pinjam:</label>
            <input type="date" name="tgl_pinjam">
        </div> -->
    	<div class="form-group">
    	<label>Tanggal Kembali(Deadline):</label>
    	<input class="form-control" type="text" name="tgl_kembali" value="<?php $deadline = strtotime('+7 day',strtotime(date('Y-m-d'))); echo date('Y-m-d',$deadline);?>" readonly>
    	</div>
        <!-- <div class="form-group">
            <label>Tanggal Kembali(Deadline):</label>
            <input type="date" name="tgl_kembali">
        </div> -->
    	<div class="row">
    		<div class="col-lg-12">
    		<input type="submit" name="submit" value="SIMPAN" class="btn btn-block btn-md btn-primary">
    		</div>
    	</div>
    	</form>
	</div>

<div class="col-lg-9">
	<h3 style="text-align: center;">Data Peminjaman</h3>
	 		<!-- <div class="panel panel-default">
                        <div class="panel-heading">
                            DataTables Advanced Tables
                        </div> -->
                        <!-- /.panel-heading -->

                        <?php
                            $notif = $this->session->flashdata('notif');

                            if(!empty($notif)){
                                echo '<div class="alert alert-success">'.$notif.'</div>';
                            }
                        ?>
                        <!-- /.panel-heading -->
            <div class="row">
                <div class="col-lg-12">
                        
                        <div class="panel panel-default">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                        	<th>No</th>
                                            <th>Nama</th>
                                            <th>Judul</th>
                                            <th>Tanggal Pinjam</th>
                                            <th>Deadline</th>
                                            <th>Petugas</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                    	$no = 1;
                                        foreach ($pinjam as $data) {
                                            $deadline = strtotime($data->DEADLINE);
                                            $sekarang = strtotime(date('Y-m-d'));
                                            $kembali = ($sekarang-$deadline)/86400;
                                            $denda = $kembali*500;

                                            if($denda < 0){
                                                $denda = 0;
                                            }

                                        echo '
                                        <tr class="odd gradeX">
                                        	<td>'.$no.'</td>
                                            <td>'.$data->NAMA.'</td>
                                            <td>'.$data->JUDUL.'</td>
                                            <td>'.$data->TANGGAL.'</td>
                                            <td>'.$data->DEADLINE.'</td>
                                            <td>'.$data->NAMAP.'</td>
                                            <td><a href="'.base_url().'index.php/pinjam/kembali/'.$data->NO_PINJAM.'/'.$denda.'" type="button" name="kembali" class="btn btn-sm btn-success">Kembali</a></td>
                                        </tr>
                                        '
                                        ; $no++;
                                        }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                    <!-- </div> -->

</div>
