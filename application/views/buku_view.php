<div class="col-lg-12">
	<h1 class="page-header">Buku</h1>
</div>
<div class="row col-lg-4">
<form method="post" action="<?php echo base_url(); ?>index.php/buku/simpan" id="form-buku" enctype="multipart/form-data">
    <h3 style="text-align: center;">Tambah Buku</h3>
    <?php
        if(!empty($notif)){
            echo '<div class="alert alert-success">';
            echo $notif;
            echo '</div>';
        }
    ?>
    	<div class="form-group">
    		<label for="judul buku">Judul Buku:</label>
    		<input type="text" class="form-control" name="judul" placeholder="Judul Buku" required>
    	</div>
        <div class="form-group">
            <label for="judul buku">Kategori:</label>
            <input type="text" class="form-control" name="kategori" placeholder="Kategori" required>
        </div>
    	<div class="form-group">
    		<label for="penulis">Penulis:</label>
    		<input type="text" class="form-control" name="penulis" placeholder="Penulis" required>
    	</div>
        <div class="form-group">
            <label for="penulis">Penerbit:</label>
            <input type="text" class="form-control" name="penerbit" placeholder="Penerbit" required>
        </div>
        <div class="form-group">
            <label for="penulis">Jumlah:</label>
            <input type="text" class="form-control" name="jumlah" placeholder="Jumlah" required>
        </div>
        <div class="form-group">
            <label for="penulis">Foto:</label>
            <input type="file" class="form-control" name="foto" required>
        </div>
    	<div class="row">
    		<div class="col-lg-6">
    		<input type="reset" name="reset" value="RESET" class="btn btn-block btn-md btn-danger">
    		</div>
    		<div class="col-lg-6">
    		<input type="submit" name="submit" value="SIMPAN" class="btn btn-block btn-md btn-primary">
    		</div>
    	</div>
    	</form>
	</div>

<div class="col-lg-8">
	<h3 style="text-align: center;">Data Buku</h3>
    <?php
        $notif = $this->session->flashdata('notif');

        if(!empty($notif)){
        echo '<div class="alert alert-success">'.$notif.'</div>';
        }
    ?>
<div class="col-lg-12">
	 		<!-- <div class="panel panel-default">
                        <div class="panel-heading">
                            DataTables Advanced Tables
                        </div> -->
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Judul</th>
                                            <th>Kategori</th>
                                            <th>Penulis</th>
                                            <th>Penerbit</th>
                                            <th>Stock</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                        $no = 1;
                                        foreach ($buku as $data) {
                                        echo "
                                            <tr class='odd gradeX'>
                                            <td>".$no++."</td>
                                            <td>$data->JUDUL</td>
                                            <td>$data->KATEGORI</td>
                                            <td>$data->PENULIS</td>
                                            <td>$data->PENERBIT</td>
                                            <td>$data->JUMLAH</td>
                                            <td><a class='btn btn-primary glyphicon glyphicon-picture' rel='popover' data-img='".base_url()."/uploads/".$data->FOTO."'></a>
                                            <button data-toggle='modal' data-target='#modal$data->KD_BUKU' class='glyphicon glyphicon-edit btn btn-success'></button>
                                            <a href='".base_url()."index.php/buku/hapus/$data->KD_BUKU' class='glyphicon glyphicon-trash btn btn-danger'></a></td>
                                            </tr>

                                            <!-- Modal -->
                                        <div class='modal fade' id='modal$data->KD_BUKU' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
                                            <div class='modal-dialog'>
                                                <div class='modal-content'>
                                                  <form role='form' action='".base_url()."index.php/buku/edit/".$data->KD_BUKU."' method='post'>
                                                    <div class='modal-header'>
                                                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                                        <h4 class='modal-title' id='myModalLabel'>Edit Data Buku</h4>
                                                    </div>
                                                    <div class='modal-body'>";?>
                                                      <div class="form-group">
                                                        <label>Judul</label>
                                                        <input class="form-control" type="text" name="judul" required value="<?php echo $data->JUDUL; ?>">
                                                      </div>
                                                      <div class="form-group">
                                                        <label>Kategori</label>
                                                        <input class="form-control" type="text" name="kategori" required value="<?php echo $data->KATEGORI; ?>">
                                                      </div>
                                                      <div class="form-group">
                                                        <label>Penulis</label>
                                                        <input class="form-control" type="text" name="penulis" required value="<?php echo $data->PENULIS; ?>">
                                                      </div>
                                                      <div class="form-group">
                                                        <label>Penerbit</label>
                                                        <input class="form-control" type="text" name="penerbit" required value="<?php echo $data->PENERBIT; ?>">
                                                      </div>
                                                      <div class="form-group">
                                                        <label>Jumlah</label>
                                                        <input class="form-control" type="text" name="penerbit" required value="<?php echo $data->JUMLAH; ?>">
                                                      </div>
                                                    <?php echo "</div>
                                                    <div class='modal-footer'>
                                                    <div class='row'>
                                                    <div class='col-lg-6'>
                                                    <input type='button' name='cancel' class='btn btn-danger btn-block' data-dismiss='modal' value='Cancel'>
                                                    </div>
                                                    <div class='col-lg-6'>
                                                    <input type='submit' name='submit' class='btn btn-success btn-block' value='Edit'>
                                                    </div>
                                                </div>

                                                    </form>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <!-- /.modal -->

                                      ";
                                    }
                                  ?>
                                        
                                    
                        
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    <!-- </div> -->
</div>
