 	<div class="col-lg-12">
        <h1 class="page-header">Anggota</h1>
    </div>
    <div class="row col-lg-4">
    <form method="post" action="<?php echo base_url(); ?>index.php/anggota/daftar" id="form-buku">
    <h3 style="text-align: center;">Tambah Anggota</h3>
     <?php
        $notif = $this->session->flashdata('notif1');

        if(!empty($notif)){
        	echo '<div class="alert alert-success">';
        	echo $notif;
        	echo '</div>';
        }
    ?>
    	<div class="form-group">
    		<label for="nis">NIS:</label>
    		<input type="text" class="form-control" name="nis" placeholder="NIS" required>
    	</div>
    	<div class="form-group">
    		<label for="nis">Nama:</label>
    		<input type="text" class="form-control" name="nama" placeholder="Nama" required>
    	</div>
    	<div class="form-group">
            <label for="kelas">Kelas:</label>
            <select class="form-control" name="kelas">
            <option value="X">X</option>
            <option value="XI">XI</option>
            <option value="XII">XII</option>
            </select>
        </div>
    	<div class="form-group">
    		<label for="jk">Jenis Kelamin:</label>
    		<select class="form-control" name="jk">
    		<option value="L">Laki-Laki</option>
    		<option value="P">Perempuan</option>
    		</select>
    	</div>
    	<div class="form-group">
    		<label for="nohp">No Hp:</label>
    		<input type="text" class="form-control" name="nohp" placeholder="No Hp" required>
    	</div>
    	<div class="row">
    		<div class="col-lg-6">
    		<input type="reset" name="reset" value="RESET" class="btn btn-block btn-md btn-danger">
    		</div>
    		<div class="col-lg-6">
    		<input type="submit" name="submit" value="SIMPAN" class="btn btn-block btn-md btn-primary">
    		</div>
    	</div>
    	</form>
	</div>

<div class="col-lg-8">
	<h3 style="text-align: center;">Data Anggota</h3>
	 		<!-- <div class="panel panel-default">
                        <div class="panel-heading">
                            DataTables Advanced Tables
                        </div> -->
                        <!-- /.panel-heading -->

                        <?php
                            $notif = $this->session->flashdata('notif');

                            if(!empty($notif)){
                                echo '<div class="alert alert-success">'.$notif.'</div>';
                            }
                        ?>
                        <!-- /.panel-heading -->
            <div class="row">
                <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>NIS</th>
                                            <th>Nama</th>
                                            <th>Kelas</th>
                                            <th>JK</th>
                                            <th>No Hp</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                    $no = 1;
                                    foreach ($anggota as $user) {
                                      echo "
                                        <tr>
                                          <td>".$no++."</td>  
                                          <td>$user->NIS</td>
                                          <td>$user->NAMA</td>
                                          <td>$user->KELAS</td>
                                          <td>"; if($user->JK=='L'){echo 'Laki-laki';}else{echo 'Perempuan';} echo "</td>
                                          <td>$user->NO_HP</td>
                                          <td>
                                            <button class='btn btn-success glyphicon glyphicon-edit' data-toggle='modal' data-target='#modal$user->ID_USER'></button>
                                            <a href='".base_url()."index.php/anggota/hapus/$user->ID_USER' type='button' class='btn btn-danger glyphicon glyphicon-trash'></a>
                                          </td>
                                        </tr>
                                        <!-- Modal -->
                                        <div class='modal fade' id='modal$user->ID_USER' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
                                            <div class='modal-dialog'>
                                                <div class='modal-content'>
                                                  <form role='form' action='".base_url()."index.php/anggota/edit/".$user->ID_USER."' method='post'>
                                                    <div class='modal-header'>
                                                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                                        <h4 class='modal-title' id='myModalLabel'>Edit Data Anggota</h4>
                                                    </div>
                                                    <div class='modal-body'>";?>
                                                      <div class="form-group">
                                                        <label>NIS</label>
                                                        <input class="form-control" type="text" name="nis" required value="<?php echo $user->NIS; ?>">
                                                      </div>
                                                      <div class="form-group">
                                                        <label>Nama</label>
                                                        <input class="form-control" type="text" name="nama" required value="<?php echo $user->NAMA; ?>">
                                                      </div>
                                                      <div class="form-group">
                                                        <label>Kelas</label>
                                                        <input class="form-control" type="text" name="kelas" required value="<?php echo $user->KELAS; ?>">
                                                      </div>
                                                      <div class="form-group">
                                                        <label>Jenis Kelamin</label>
                                                        <select class="form-control" name="jk" required>
                                                          <option value="L">Laki-laki</option>
                                                          <option value="P" <?php if($user->JK=='P'){echo 'selected';} ?>>Perempuan</option>
                                                        </select>
                                                      </div>
                                                      <div class="form-group">
                                                        <label>Nomor HP</label>
                                                        <input class="form-control" type="number" name="nohp" min="0" required value="<?php echo $user->NO_HP; ?>">
                                                      </div>
                                                    <?php echo "</div>
                                                    <div class='modal-footer'>
                                                    <div class='row'>
                                                    <div class='col-lg-6'>
                                                    <input type='button' name='cancel' class='btn btn-danger btn-block' data-dismiss='modal' value='Cancel'>
                                                    </div>
                                                    <div class='col-lg-6'>
                                                    <input type='submit' name='submit' class='btn btn-success btn-block' value='Edit'>
                                                    </div>
                                                </div>

                                                    </form>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <!-- /.modal -->
                                      ";
                                    }
                                  ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                    <!-- </div> -->

</div>
