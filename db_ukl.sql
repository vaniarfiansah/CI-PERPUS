-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 22 Apr 2018 pada 15.10
-- Versi Server: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ukl`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `anggota`
--

CREATE TABLE `anggota` (
  `ID_USER` int(50) NOT NULL,
  `NIS` mediumtext,
  `NAMA` mediumtext,
  `KELAS` mediumtext,
  `JK` char(1) DEFAULT NULL,
  `NO_HP` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `anggota`
--

INSERT INTO `anggota` (`ID_USER`, `NIS`, `NAMA`, `KELAS`, `JK`, `NO_HP`) VALUES
(1, '43212', 'Deva', 'X', '1', '09876543'),
(2, '09121224', 'Budi', 'X', '1', '081435332'),
(3, '091781367', 'Mia', 'X', '0', '0878164861'),
(4, '09113896', 'Made', 'XI', '1', '0813413513'),
(5, '09134113', 'Bunga', 'XI', '0', '08139846'),
(6, '098757875', 'Cika', 'XII', 'P', '09876543'),
(13, '1405667', 'Qurrotul ', 'XII', 'P', '085331727667'),
(14, '1804', 'Rizaldi Wahaz', 'XI', 'L', '087752767620');

-- --------------------------------------------------------

--
-- Struktur dari tabel `buku`
--

CREATE TABLE `buku` (
  `KD_BUKU` int(50) NOT NULL,
  `BARCODE` int(11) DEFAULT NULL,
  `JUDUL` varchar(50) NOT NULL,
  `KATEGORI` mediumtext,
  `PENULIS` mediumtext,
  `PENERBIT` mediumtext,
  `JUMLAH` int(11) DEFAULT NULL,
  `FOTO` text NOT NULL,
  `DIPINJAM` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `buku`
--

INSERT INTO `buku` (`KD_BUKU`, `BARCODE`, `JUDUL`, `KATEGORI`, `PENULIS`, `PENERBIT`, `JUMLAH`, `FOTO`, `DIPINJAM`) VALUES
(20, NULL, 'Puisi', 'Fiksi', 'Sapadi ', 'GMC ', 2, '16818-7-buku-puisi-indonesia-yang-wajib-kita-baca-1.jpg', NULL),
(21, NULL, 'Naruto ', 'Komik', 'Ifan ', 'Ibizz', 42, 'Volume05Cover.jpg', NULL),
(23, NULL, 'Rindu', 'Fiksi', 'Tere Liye', 'Graha', 7, '94320_b.jpg', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas`
--

CREATE TABLE `petugas` (
  `ID_PETUGAS` int(50) NOT NULL,
  `USERNAME` mediumtext,
  `PASSWORD` mediumtext,
  `NAMAP` mediumtext,
  `ROLE` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `petugas`
--

INSERT INTO `petugas` (`ID_PETUGAS`, `USERNAME`, `PASSWORD`, `NAMAP`, `ROLE`) VALUES
(1, 'antok', '123', 'Antok', 'super'),
(3, 'bejo', '123', 'Bejo', 'super'),
(4, 'bagus', '123', 'Bagus', 'super'),
(5, 'nani', '123', 'Nani', 'admin'),
(8, 'ayun', '8888', 'Ayun', 'super');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pinjam`
--

CREATE TABLE `pinjam` (
  `NO_PINJAM` int(50) NOT NULL,
  `ID_PETUGAS` int(11) DEFAULT NULL,
  `ID_USER` int(11) DEFAULT NULL,
  `KD_BUKU` int(50) NOT NULL,
  `TANGGAL` date DEFAULT NULL,
  `KEMBALI` date NOT NULL,
  `DEADLINE` date DEFAULT NULL,
  `DENDA` int(11) DEFAULT NULL,
  `STATUS` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pinjam`
--

INSERT INTO `pinjam` (`NO_PINJAM`, `ID_PETUGAS`, `ID_USER`, `KD_BUKU`, `TANGGAL`, `KEMBALI`, `DEADLINE`, `DENDA`, `STATUS`) VALUES
(69, 1, 1, 15, '2017-07-05', '2017-07-20', '2017-07-12', 4000, 'Terlambat'),
(70, 1, 6, 21, '2017-07-13', '0000-00-00', '2017-07-20', 0, 'Belum Kembali'),
(71, 1, 13, 23, '2017-07-10', '2017-07-19', '2017-07-17', 1000, 'Terlambat'),
(72, 1, 14, 16, '2017-07-19', '2017-07-19', '2017-07-26', 0, 'Tepat Waktu'),
(73, 1, 2, 25, '2017-07-06', '0000-00-00', '2017-07-13', 0, 'Belum Kembali'),
(74, 1, 5, 25, '2017-07-13', '0000-00-00', '2017-07-20', 0, 'Belum Kembali'),
(75, 1, 4, 21, '2017-07-17', '0000-00-00', '2017-07-24', 0, 'Belum Kembali'),
(76, 1, 14, 16, '2017-07-04', '2017-07-19', '2017-07-11', 4000, 'Terlambat'),
(77, 1, 3, 23, '2017-07-10', '2017-07-19', '2017-07-17', 1000, 'Terlambat'),
(78, 1, 2, 22, '2017-07-13', '2017-07-19', '2017-07-20', 0, 'Tepat Waktu'),
(79, 1, 4, 20, '2017-07-20', '0000-00-00', '2017-07-27', 0, 'Belum Kembali'),
(80, 1, 5, 23, '2017-07-19', '2017-07-19', '2017-07-26', 0, 'Tepat Waktu'),
(81, 8, 5, 22, '2017-07-19', '0000-00-00', '2017-07-26', 0, 'Belum Kembali'),
(82, 8, 13, 16, '2017-07-05', '0000-00-00', '2017-07-12', 0, 'Belum Kembali'),
(83, 8, 3, 15, '2017-07-11', '0000-00-00', '2017-07-18', 0, 'Belum Kembali'),
(84, 8, 13, 25, '2017-07-14', '0000-00-00', '2017-07-21', 0, 'Belum Kembali'),
(85, 1, 1, 15, '2017-07-20', '0000-00-00', '2017-07-27', 0, 'Belum Kembali'),
(86, 1, 1, 21, '2017-07-20', '0000-00-00', '2017-07-27', 0, 'Belum Kembali'),
(87, 1, 14, 21, '2017-07-20', '2017-09-07', '2017-07-27', 21000, 'Terlambat');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`ID_USER`),
  ADD UNIQUE KEY `ANGGOTA_PK` (`ID_USER`);

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`KD_BUKU`),
  ADD UNIQUE KEY `BUKU_PK` (`KD_BUKU`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`ID_PETUGAS`),
  ADD UNIQUE KEY `PETUGAS_PK` (`ID_PETUGAS`);

--
-- Indexes for table `pinjam`
--
ALTER TABLE `pinjam`
  ADD PRIMARY KEY (`NO_PINJAM`),
  ADD KEY `KD_BUKU` (`KD_BUKU`),
  ADD KEY `ID_PETUGAS` (`ID_PETUGAS`),
  ADD KEY `ID_USER` (`ID_USER`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anggota`
--
ALTER TABLE `anggota`
  MODIFY `ID_USER` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `buku`
--
ALTER TABLE `buku`
  MODIFY `KD_BUKU` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `ID_PETUGAS` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `pinjam`
--
ALTER TABLE `pinjam`
  MODIFY `NO_PINJAM` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
